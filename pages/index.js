import React from 'react';
import Link from 'next/link';
import Layout from '../components/Layout';
import { withApollo } from '../lib/apollo';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

const HELLO_QUERY = gql`
  query HelloQuery {
    sayHello
  }
`;

const Home = () => {
  const { data, loading, error } = useQuery(HELLO_QUERY);

  return loading ? (
    <div>loading data...</div>
  ) : (
    <Layout>
      <h1>Say hello</h1>
      {data.sayHello}
    </Layout>
  );
};

export default withApollo(Home);
